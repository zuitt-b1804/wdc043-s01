package com.zuitt;

import java.util.Scanner;

public class Main {

    public static void main(String[] args){

        Scanner scannerName = new Scanner(System.in);

        // User's name
        System.out.println("First Name:");
        String firstName = scannerName.nextLine();
        System.out.println("Last Name:");
        String lastName = scannerName.nextLine();

        // User's grades
        System.out.println("First Subject Grade:");
        double firstSubject = scannerName.nextDouble();
        System.out.println("Second Subject Grade:");
        double secondSubject = scannerName.nextDouble();
        System.out.println("Third Subject Grade:");
        double thirdSubject = scannerName.nextDouble();

        //Average
        double average=(firstSubject+secondSubject+thirdSubject)/3;
        //to convert it to integer
        //int averageInt = (int)average;

        System.out.println("Good Day, "+firstName+" "+lastName);
        System.out.println("Your Average is: " +average);

    }

}
